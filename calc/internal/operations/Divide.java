package org.eclipse.example.calc.internal.operations;

import org.eclipse.example.calc.Operation;

public class Divide extends AbstractOperation implements Operation{
	@Override
	public String getName() {
		return "/";
		
	}
}